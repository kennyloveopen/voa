define(['backbone', 'marionette', 'jquery', 'jquerymobile', 'hbs!templates/mobileHeader'],
    function (Backbone, Marionette, $, jqm, template) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function() {
                _.bindAll(this);
            },
            events:{
            
            "click a":"navigate"
           },
        navigate: function(e){
        e.preventDefault();
       Backbone.history.navigate( $(e.currentTarget).attr("href"),{trigger: true});
          },
                                                   
                                                   
            onRender: function() {
                this.$el.navbar();
            }
        });
    });