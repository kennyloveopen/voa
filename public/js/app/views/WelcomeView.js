define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/welcome','hbs!templates/deskleft','hbs!templates/deskmain','hbs!templates/deskitem','hbs!templates/deskdetail'],
    function(App, Backbone, Marionette, $, Model, template,deskleft,deskmain,deskitem,deskdetail) {
        //ItemView provides some default rendering logic
        var WelcomeView=WelcomeView||{};
       WelcomeView.LeftView=Backbone.Marionette.ItemView.extend({

            template: deskleft,

             events: {
         "click a": "detailList"
        },

           detailList:function(e){

               WelcomeView.trigger(e);

       }


       });
        WelcomeView.ItemView=Backbone.Marionette.ItemView.extend({

            template: deskitem,



           detailList:function(e){

               WelcomeView.trigger(e,this.model);

       },

            onRender:function(){

              
                // Get rid of that pesky wrapping-div.
                // Assumes 1 child element present in template.
                this.$el = this.$el.children();
                // Unwrap the element to prevent infinitely
                // nesting elements during re-render.
                this.$el.unwrap();
                this.setElement(this.$el);

            }


       });
       WelcomeView.RightView=Backbone.Marionette.CompositeView.extend({

            template: deskmain,
            childView: WelcomeView.ItemView,
           childViewContainer:"#item-list"

       });





         WelcomeView.MainView =  Backbone.Marionette.LayoutView.extend({
             template: template,

             regions: {
                leftPanle: "#left-panle",
                rightPanle: "#right-panle"
               }

        });


         WelcomeView.DetailView =  Backbone.Marionette.ItemView.extend({
             template: deskdetail


        });

    WelcomeView.trigger=function(e, modle){
         e.preventDefault();
         var currentUrl=$(e.currentTarget).attr("href");
           if(currentUrl.indexOf("#detail")!=-1){

                Backbone.history.navigate(currentUrl,{trigger: true});
           }else{
             Backbone.history.navigate("#detaillist"+currentUrl,{trigger: true});

           }

    };

     return WelcomeView;
    });
