
define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/item','hbs!templates/list'],
    function(App, Backbone, Marionette, $, Model, item,list) {
	var Views ={};
	Views.ItemView = Marionette.ItemView.extend({
		template: item,

        events: {
      "click a": "detailList"
       },

       detailList:function(e){

           e.preventDefault();
           if(window.location.href.indexOf("detaillist")!=-1){

                Backbone.history.navigate("#detail/"+this.model.get("id"),{trigger: true});
           }else{
             Backbone.history.navigate("#detaillist/"+this.model.get("id"),{trigger: true});

           }
       }
	});

	// Item List View
	// --------------
	//
	// Controls the rendering of the list of items, including the
	// filtering of activs vs completed items for display.
	Views.ListView = Backbone.Marionette.CompositeView.extend({
		template: list,
		childView: Views.ItemView,
         childViewContainer: '#item-list'
	});


 return Views;

});
