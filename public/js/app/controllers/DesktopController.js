define(['App', 'backbone', 'marionette', 'views/WelcomeView', 'views/DesktopHeaderView','views/LoadView',"models/Model","collections/Collection","models/Detail","jquery"],
    function (App, Backbone, Marionette, WelcomeView, DesktopHeaderView,LoadView,Model,Collection,Detail,$) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {
            App.headerRegion.show(new DesktopHeaderView());
        },
        //gets mapped to in AppRouter's appRoutes
        index:function () {
            console.log(WelcomeView);
               var welcomeView = new WelcomeView.MainView();
              App.mainRegion.show(welcomeView);
            var leftView = new WelcomeView.LeftView();
       
            var collection = new Collection();
            collection.url="/api/Technology_Report_1.html";
                welcomeView.getRegion("leftPanle").show(leftView);
     
                 App.MainView=welcomeView;
            var promise = this.fetchData(collection);
            $.when(promise).done(function(list){
               
                if(list!==undefined){
                    
                var rightView = new WelcomeView.RightView({collection:collection});
                 welcomeView.getRegion("rightPanle").show(rightView);
                 App.currentCollection=collection;
              
                    
            }else{
                
                   App.MainView.getRegion("rightPanle").show(new LoadView.missData());
                
            }

            });
          
        },
        stindex:function(){
            
        },
        detaillist:function(id){
             console.log("detaillist");
             console.log(id);
         
        
           var url ="/api/"+id;
                console.log(url);
               var collection= new Collection();
            collection.url=url;
           
             
              
            var promise = this.fetchData(collection);
            $.when(promise).done(function(list){
               
                if(list!==undefined){
                    

                App.currentCollection=collection;
                var itemList = new WelcomeView.RightView({collection:collection});

                 App.MainView.getRegion("rightPanle").show(itemList);

                    
                }else{
                     App.MainView.getRegion("rightPanle").show(new LoadView.missData());
                    
                }
                
    });
          
            
          
        },
        detail:function(id,value){
            
              console.log("detail");
           
        //  var model = App.currentCollection.get(id);
         
               var url ="/detail";
            if(id){
              
                url=url+"/"+id;

              }
             if(value){

                 url=url+"/"+value;
             }
        
            console.log(url);
             var newmodel = new Detail();
             newmodel.url=url;
            
             var promise = this.fetchData(newmodel);
            $.when(promise).done(function(data){
            
                 if(data!==undefined){
                     var detailView = new  WelcomeView.DetailView({model: newmodel});
                     console.log(detailView.model);
                    App.MainView.getRegion("rightPanle").show(detailView);
                }else{
                    
                    App.MainView.getRegion("rightPanle").show(new LoadView.missData());
                    
                }
                 
             });
        
       },
        
        fetchData:function(model){
         
            var loadingView = new LoadView.Loading();
            if(App.MainView){
               App.MainView.getRegion("rightPanle").show(loadingView);  
            }else
            {
                 var welcomeView = new WelcomeView.MainView();
                 App.mainRegion.show(welcomeView);
                var leftView = new WelcomeView.LeftView();
       
                  welcomeView.getRegion("rightPanle").show(loadingView);
                  welcomeView.getRegion("leftPanle").show(leftView);
                  App.MainView=welcomeView;
            }
           
            
             var defer = $.Deferred();
                 model.fetch({
                  success: function(data){
                    defer.resolve(data);
                  },
                   error: function(data){
                      defer.resolve(undefined);
                    }
                });
            var promise = defer.promise();

            return promise;
        
        
    }
        
    });
    
    
});