define(['App', 'backbone', 'marionette', 'views/listView', 'views/MobileHeaderView',"collections/Collection","menu","models/Detail","views/DetailView"],
    function (App, Backbone, Marionette, listView, MobileHeaderView,Collection,menu,Detail,DetailView) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {
           
            App.headerRegion.show(new MobileHeaderView());
        },
        //gets mapped to in AppRouter's appRoutes
        index:function () {
			var c = new Collection();
			c.add(JSON.parse(menu.index));
		
            var itemList = new listView.ListView({collection:c});
			console.log(itemList.collection);
            App.mainRegion.show(itemList);
            App.currentCollection=c;
        },
           //gets mapped to in AppRouter's appRoutes
        stindex:function () {
			var c = new Collection();
			c.add(JSON.parse(menu.stindex));
		
            var itemList = new listView.ListView({collection:c});
			console.log(itemList.collection);
            App.mainRegion.show(itemList);
            App.currentCollection=c;
        },
         detaillist:function(id){
           console.log("detaillist");
             console.log(id);
           var model = App.currentCollection.get(id);
         require(["views/listView","collections/Collection"],function(listView,Collection){
           var url ="/api"+ model.get("href");
                console.log(url);
               var collection= new Collection();
            collection.url=url;
            collection.fetch();
          
            
         App.currentCollection=collection;
            var itemList = new listView.ListView({collection:collection});
            App.mainRegion.show(itemList);
           
            
        });
         },
        
         detail:function(id){
           console.log("detail");
             console.log(id);
           var model = App.currentCollection.get(id);
          var url ="/detail"+ model.get("href");
            console.log(url);
             var newmodel = new Detail();
             newmodel.url=url;
             
             newmodel.fetch({
                 success:function(){
                 
                 var detailView = new DetailView({model: newmodel});
             console.log(detailView.model);
             console.log(detailView.serializeData(newmodel));
            App.mainRegion.show(detailView);
           
                 
             }
                 
             });
          //   console.log(newmodel);
             
            
       
         }
        
        
    });
});