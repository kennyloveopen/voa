define(['jquery', 'backbone', 'marionette', 'underscore', 'handlebars','routers/AppRouter'],
    function ($, Backbone, Marionette, _, Handlebars,AppRouter) {
        var App = new Backbone.Marionette.Application();

        function isMobile() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            return ((/iPhone|iPod|iPad|Android|BlackBerry|Opera Mini|IEMobile/).test(userAgent));
        }

        //Organize Application into regions corresponding to DOM elements
        //Regions can contain views, Layouts, or subregions nested as necessary
        App.addRegions({
            headerRegion:"header",
            mainRegion:"#main"
            
        });

        App.addInitializer(function () {
            Backbone.history.start();
        });

      App.getCurrentRoute = function(){
    return Backbone.history.fragment;
  };
    
 
    
    App.getMainView= function(app){
        
        if(app.MainView){
            
          return  app.MainView
            
        }
        
         var welcomeView = new WelcomeView.MainView();
          app.mainRegion.show(welcomeView);
           var leftView = new WelcomeView.LeftView();
         welcomeView.getRegion("leftPanle").show(leftView);
          app.MainView=welcomeView;
        return  welcomeView;
    }     
    
        
        App.on("list:detail",function(model){
              
        require(["views/listView","collections/Collection"],function(listView,Collection){
       
              var url ="/api"+ model.get("href");
                console.log(url);
               var collection= new Collection();
            collection.url=url;
            collection.fetch();
          
            
         
            var itemList = new listView.ListView({collection:collection});
            App.mainRegion.show(itemList);
           
            
        });
             
        });
    
     App.on("start", function(){
    if(Backbone.history){
     

        if(App.getCurrentRoute() === ""){
          Backbone.history.navigate("index", {trigger: true});
        }
  
    }
  });
    
    
        App.mobile = isMobile();

        return App;
    });