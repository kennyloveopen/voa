define(['App','backbone', 'marionette'], function(App,Backbone, Marionette) {


   var AppRouter = Backbone.Marionette.AppRouter.extend({
       //"index" must be a method in AppRouter's controller
       appRoutes: {
           "index": "index",
           "detaillist/:id" :"detaillist",
           "detail/:id/:value" :"detail",
           "stindex": "stindex",
           "detail/:id":"detail"
       }




   });



   return AppRouter;
});
