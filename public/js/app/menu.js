define( function(){
var menu={
    
    index:"[{ \"id\": 1, \"text\": \"Technology Report 科技报道\",\"href\": \"/Technology_Report_1.html\" , \"type\":\"detaillist\"}, { \"id\": 3,\"text\": \"This is America 今日美国\", \"href\": \"/This_is_America_1.html\" , \"type\":\"detaillist\"}, { \"id\": 5, \"text\": \"Agriculture Report 农业报道\",\"href\": \"/Agriculture_Report_1.html\" , \"type\":\"detaillist\"}, { \"id\": 7,\"text\": \"Science in the News 科学报道\", \"href\": \"/Science_in_the_News_1.html\" , \"type\":\"detaillist\"},{ \"id\": 9, \"text\": \"Health Report 健康报道\", \"href\": \"/Health_Report_1.html\" , \"type\":\"detaillist\"}, { \"id\": 11, \"text\": \"Explorations 自然探索\",\"href\": \"/Explorations_1.html\" , \"type\":\"detaillist\"},{ \"id\": 13, \"text\": \"Education Report 教育报道\", \"href\": \"/Education_Report_1.html\" , \"type\":\"detaillist\"},{ \"id\": 15, \"text\": \"The Making of a Nation 建国史话\", \"href\": \"/The_Making_of_a_Nation_1.html\" , \"type\":\"detaillist\"}, { \"id\": 17, \"text\": \"Economics Report 经济报道\", \"href\": \"/Economics_Report_1.html\" , \"type\":\"detaillist\"},{ \"id\": 19,\"text\": \"American Mosaic 美国万花筒\", \"href\": \"/American_Mosaic_1.html\" , \"type\":\"detaillist\"}, { \"id\": 21, \"text\": \"In the News 时事新闻\",\"href\": \"/In_the_News_1.html\" , \"type\":\"detaillist\"}, { \"id\": 23,\"text\": \"American Stories 美国故事\",\"href\": \"/American_Stories_1.html\" , \"type\":\"detaillist\"}, { \"id\": 25,\"text\": \"Words And Their Stories 词汇掌故\",\"href\": \"/Words_And_Their_Stories_1.html\" , \"type\":\"detaillist\"}, { \"id\": 27,\"text\": \"People in America 美国人物志\", \"href\": \"/People_in_America_1.html\" , \"type\":\"detaillist\"}, { \"id\": 29, \"text\": \"AS IT IS 慢速新闻杂志\", \"href\": \"/as_it_is_1.html\" , \"type\":\"detaillist\"}]",
    stindex:"[{ \"id\": 1, \"text\": \"VOA Standard English\",\"href\": \"/VOA_Standard_1.html\" , \"type\":\"detaillist\"}, { \"id\": 2, \"text\": \"VOA Standard English Archives \",\"href\": \"/VOA_Standard_1_archiver.html\" , \"type\":\"detaillist\"}]"
    
    
};

   return menu;
  }
 );