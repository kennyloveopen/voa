/**
* Created with voaTest.
* User: chenhuilove123
* Date: 2015-04-21
* Time: 08:22 AM
* To change this template use Tools | Templates.
*/
define(["jquery", "backbone"],
function($, Backbone) {
        // Creates a new Backbone Model class object
        var Detail = Backbone.Model.extend({

            // Default values for all of the Model attributes
            defaults: {
		
			title:"",
			content:"",
            mp3:""
            }

           

        });

        // Returns the Model class
        return Detail;

});